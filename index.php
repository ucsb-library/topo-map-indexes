<?php
 require("config/config.php");
require($root."/lib/functions.php");
$broad_divisions = get_broad_divisions($broad_divisions_datafile);
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>International Topo Map Indexes - UCSB Library</title>

    <link type="text/css" rel="stylesheet" media="screen" href="css/screen.css" />

<?php include($root."/lib/analytics.php") ?>
</head>

<body>
<?php include($root."/lib/navigation.php") ?>
<h1 class="title-head">International Topographical Map Indexes</h1>
<div class="text content">
    <div class="image-map">
    <img src="images/topo_broad_divisions.jpg" width="900" height="450" border="0" usemap="#topo_broad_divisions" />
    </div>
    <p>Use these pages to identify series of foreign topographic maps available in MIL.  This map will lead you to a list describing each series, as well as a link to a graphical index of the series when available.  <strong>Please note</strong>: most of these maps are paper only.  Some maps may be stored offsite and require some time to retrieve.</p>

    <ul>
<?php
foreach ($broad_divisions as $key => $value) {
    // skip the US as this is for NON-US Topos only
    if($key == 3700){continue;}
    if($key == 6700){
        $value .= " <span style=\"font-weight: normal;\">(includes Russia and former Soviet Republics)</span>";
    } // added note for Asia see Jira SUPPORT-4697
    echo '<li><a href="topos.php?sn='
       . $key . '">' . $value . '</a></li>';
}
echo '<li><a href="topos.php">All</a></li>';
?>
    </ul>
</div>

<?php include($root."/lib/footer.php") ?>

<map name="topo_broad_divisions" id="topo_broad_divisions">
  <area shape="poly" coords="2,5,7,15,11,275,96,269,164,247,188,252,197,247,204,260,228,258,306,221,298,124,305,76,306,5,233,19,203,11,175,7" href="topos.php?sn=6700" />
  <area shape="poly" coords="311,226,405,213,415,294,339,405,159,400,8,399,10,286,107,276,145,259,166,257,180,259,189,259,196,258,206,267,230,269,271,251" href="topos.php?sn=8940" />
  <area shape="poly" coords="319,14,306,104,332,199,418,194,483,197,526,175,518,160,551,170,640,123,657,62,695,40,694,11" href="topos.php?sn=3300" />
  <area shape="poly" coords="708,13,714,29,702,48,668,62,667,82,679,138,733,134,762,129,772,138,799,138,802,123,822,116,784,81,798,72,825,60,827,39,821,17" href="topos.php?sn=5550" />
  <area shape="poly" coords="829,18,829,31,832,43,834,58,827,66,793,83,820,109,828,119,805,127,808,141,820,149,829,172,844,197,865,192,885,186,895,162,894,15" href="topos.php?sn=6700" />
  <area shape="poly" coords="803,142,809,150,817,161,827,180,838,199,842,203,870,195,892,203,892,286,837,330,772,336,734,316,737,231,683,218,661,188,667,166,669,143,754,137,772,148" href="topos.php?sn=7800" />
  <area shape="poly" coords="629,140,599,154,560,172,548,173,526,166,530,178,508,195,475,206,451,214,457,273,493,300,506,360,538,393,643,399,655,320,685,238,654,194,655,154" href="topos.php?sn=4330" />
</map>
</body>
</html>
