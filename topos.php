<?php
include("config/config.php");
include("lib/functions.php");

$broad_divisions = get_broad_divisions($broad_divisions_datafile);

include("lib/doctype.php");

if(isset($_GET['sn'])){
  $sn = intval($_GET['sn']);
  $specific_page_title = " - " . $broad_divisions[$sn];
}else{
  $specific_page_title = "";
}

function primo_link($call_number){
  /*   pass in a string with the call number
       returns a string with a primo search URL for the call number
       cleans the data and turns it into a URL       
  */
  $call_number = str_ireplace('annex', '', $call_number);
  $call_number = str_ireplace('[annex]', '', $call_number);
  $call_number = str_ireplace('reference', '', $call_number);
  $call_number = trim($call_number);
  $url = 'https://ucsb-primo.hosted.exlibrisgroup.com/primo-explore/search?query=lsr04,contains,'
      . urlencode($call_number) . ',AND&tab=default_tab&search_scope=default_scope&sortby=rank&vid=UCSB&mode=advanced&offset=0)';
  return $url;
}

echo "<title>International Topo Map Indexes $specific_page_title - UCSB Library</title>";
echo '<link type="text/css" rel="stylesheet" href="css/tablesaw-1.0.4.css" />';
echo '<link type="text/css" rel="stylesheet" media="screen" href="css/screen.css" />';

//
include($root."/lib/analytics.php");
//

echo '</head>'
  . '<body>';

//
include($root."/lib/navigation.php");
//

echo "<h1 class=\"title-head\">International Topographical Map Indexes $specific_page_title</h1>";

echo '<div class="content">';
echo '<p class="text">All of these maps are available at MIL.  Some maps may be stored offsite and will require time to retrieve.  Please inquire with MIL staff at 805-893-2779 or <a href="mailto:milrefdesk@library.ucsb.edu">milrefdesk@library.ucsb.edu</a>.</p>';

echo '<table cellspacing="0" class="tablesaw" data-tablesaw-sortable data-tablesaw-sortable-switch>';

if (!($handle = fopen($topos_datafile, "r"))) {
  echo '<h2>Unable to read data.</h2>';
}
else {
  echo '<thead>'
    . '<tr class="column_header">'
    . '<th class="finding-aid" data-tablesaw-sortable-col>Finding Aid</th>'
    . '<th class="country"  data-tablesaw-sortable-default-col data-tablesaw-sortable-col>Country</th>'
    . '<th class="series-title" data-tablesaw-sortable-col>Series # and/or Title</th>'
    . '<th class="call-number" data-tablesaw-sortable-col>Call # (ANNEX is not part of call#)</th>'
    . '<th class="scale" data-tablesaw-sortable-col>Scale</th>'
    . '<th class="author-publisher" data-tablesaw-sortable-col>Author / Publisher</th>'
    . '<th class="date" data-tablesaw-sortable-col>Begin Date - End Date</th>'
    . '<th class="percentage" data-tablesaw-sortable-col>% of Country Covered</th>'
    . '<th class="folders-drawers" data-tablesaw-sortable-col># of Folders or drawers</th>'
    . '<th class="comments" data-tablesaw-sortable-col>Comments</th>'
    . '</tr>'
    . '</thead>'
    . '<tbody>';


// read listing of pdf files from database
$db = new SQLite3('topo-index-scans_sqlite.db');
$results = $db->query('SELECT name FROM pdf_files;');

while ($row = $results->fetchArray()) {
  $pdf_files[] = $row['name'];
  }

// iterate over all rows of data
  while ($data = fgetcsv($handle, 0, ",")) {
    // skip CSV headers
    if ( $data[3] === "Scale" ||
         // skip header type spreadsheet rows which are distinguished by having no entry in the Scale column
         $data[3] === "" ) {
      continue;
    }

    $data['broad_division'] = get_division($data, $broad_divisions);

    // if a sn query param is set restrict display to only rows matching that broad division
    if ( isset($_GET['sn']) &&
         $_GET['sn'] != $data['broad_division'] ) {
      continue;
    }

    // check for the existence of the PDF file so we can avoid building links that will only result in 404 errors
    $data['finding_aid_file'] = $topo_index_scans_s3 ."/".$data[14].".pdf";
    $data['finding_aid_file_exists'] = in_array($data[14].".pdf", $pdf_files);

    // encode ampersands etc
    $data[1] = htmlspecialchars($data[1]);// country
    $data[3] = htmlspecialchars($data[3]);// Scale
    $data[4] = htmlspecialchars($data[4]);// Series or Title
    $data[5] = htmlspecialchars($data[5]);// number of folders
    $data[6] = htmlspecialchars($data[6]);// % of coverage
    $data[7] = htmlspecialchars($data[7]);// dates
    $data[14] = htmlspecialchars($data[14]);// file name stub
    $data['catalogURL'] = buildCatalogURL($data[10]);// catalog-url

    echo '<tr>';

//  Column 1 - Finding Aid Image/Link
    echo '<td class="finding-aid">';
    if ($data['finding_aid_file_exists']) { 
      echo '<a href="'
        . $topo_index_scans_s3
        . $data[14] . '.pdf">';
      echo '<img src="images/map.png" alt="PDF finding aid for '
        . $data[4] . '" title="PDF finding aid for ' . $data[4] . '" />';
      echo '</a>';
    }
    echo '</td>';
//  Column 2 - Country
    echo '<td class="country">'
      . $data[1]
      . '</td>';
//  Column 3 - Series and/or Title
    echo '<td class="series-title">';
    if($data['finding_aid_file_exists']){
      echo '<a href="' . $topo_index_scans_s3 . $data[14] . '.pdf">' . $data[4] . '</a>';
    }else{
      echo $data[4];
    }
    echo '</td>';
//  Column 4 - Call Number
    $primo_link = primo_link($data[2]);
    echo '<td class="call-number"><a href="'.$primo_link.'">'.$data[2].'</a></td>';
//  Column 5 - Scale
    echo '<td class="scale">'
      . $data[3] . '</td>';
//  Column 6 - Author/Publisher
    echo '<td class="author-publisher">'
      . $data[7] . '</td>';
//  Column 7 -  Begin - End Dates
    echo '<td class="date">'
      . $data[8] . '</td>';
//  Column 8 - Percentage of country covered
    echo '<td class="percentage">'
      . $data[6] . '</td>';
//  Column 9 - Number of Folders or Drawers
    echo '<td class="folders-drawers">'
      . $data[5] . '</td>';
// Column 10 - Sysnum / Catalog URL.  Skipped
// Column 11 - Comments
    echo '<td class="comments">'
      . $data[12] . '</td>';
    echo '</tr>';

  } // end while
} // end if

echo '</tbody>'
  . '</table>'
  . '</div> <!-- #content -->';

include($root."/lib/footer.php");

echo '<!--[if lt IE 9]><script src="//cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script><!--<![endif]-->';
echo '<script src="//cdn.jsdelivr.net/jquery/2.1.4/jquery.min.js" type="text/javascript"></script>';
// can't be async defer-ed
echo '<script src="js/stickytableheaders-0.1.19.js" type="text/javascript"></script>';
echo '<script src="js/tablesaw-1.0.4-ucsb.js" type="text/javascript"></script>';
echo '<script type="text/javascript">$( \'table\' ).stickyTableHeaders();</script>';

echo '</body>'
  . '</html>';
