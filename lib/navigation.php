<nav>
    <a class="library" href="http://www.library.ucsb.edu"><img src="/images/ucsb_logo.png" alt="UCSB Library Logo" /><span class="hidden">UCSB Library Home Page</span></a>
    <a class="mil" title="MIL Home Page" href="http://www.library.ucsb.edu/mil"><img src="/images/mil_logo.png" alt="MIL Logo" /><span class="hidden">MIL Home Page</span></a>
    <!-- Menu items -->
    <ul>
	<li class="first"><a href="<?php echo  $approot ?>">Topo Index Home</a></li>
	<li><a href="topos.php?sn=3200"><?php echo  $broad_divisions[3200] ?></a></li>
	<li><a href="topos.php?sn=3300"><?php echo  $broad_divisions[3300] ?></a></li>
	<li><a href="topos.php?sn=4330"><?php echo  $broad_divisions[4330] ?></a></li>
	<li><a href="topos.php?sn=5550"><?php echo  $broad_divisions[5550] ?></a></li>
	<li><a href="topos.php?sn=6700" title="includes Russia and former Soviet Republics"><?php echo  $broad_divisions[6700] ?></a></li>
	<li><a href="topos.php?sn=7800"><?php echo  $broad_divisions[7800] ?></a></li>
	<li><a href="topos.php?sn=8940"><?php echo  $broad_divisions[8940] ?></a></li>
	<li><a href="topos.php">All</a></li>
	<li class="last"><a href="http:///www.library.ucsb.edu/contact">Contact</a></li>
    </ul>
</nav>