<?php

//  parse CSV file into an array of all the divisions
function get_broad_divisions($broad_divisions_datafile){
  if (($handle = fopen($broad_divisions_datafile, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 9000, ",")) !== FALSE) {
      foreach ($data as $key => $value) {
        if(!is_numeric($data[0])){continue;}
        $bdsn = $data[0];
        $broad_divisions[$bdsn] = $data[1];
      }
    }
    fclose($handle);
  } else {
    die ("No data file found.");
  }
  return $broad_divisions;
}


//
// Given an array of row data figure out what broad division the record belongs to
function get_division($row_data, $broad_divisions){
  // Extract the country code from the call number
  $country_code = intval(substr($row_data[2], 0, 4));

  // Round down until we match a broad division
  while ($country_code) {
    $matched = false;
    foreach ($broad_divisions as $broad_code => $location) {
      if ($country_code == $broad_code) {
        $matched = true;
        break;
      }
    }
    if ($matched) { break; }
    $country_code--;
  }

  return ($country_code > 0) ? (string)$country_code : "3200";
}

// Parse for Aleph System numbers and build URLs
function buildCatalogURL($data){
  /*
  Sample Data: 
  3289612
  Suppressed provisional for Series: 3403014; Individual Sheets: 3051429; 3285062; 3285074; 3285093; 3285315; 3285316; 3285323; 3285330; 3285341; 3285345; 3285347; 3285384; 3285399; 3285824; 3285829; 3285853; 3291747; 3291957; 
  3049268; 3048126

  Example URL:  http://pegasus.library.ucsb.edu/F/?func=direct&doc_number=003241376&local_base=SBA01

  System numbers are 10 digits
  */
  $data = trim($data);

  $sysnumfound = preg_match("/[[:digit:]]{7,9}/", $data, $matches);
  if($sysnumfound){
    $sysnum = $matches[0];
  }else{
    $sysnum = "";
  }


  if($sysnum != ""){
    $url = "<a href=\"http://pegasus.library.ucsb.edu/F/?func=direct&doc_number=".$sysnum."&local_base=SBA01\" target=\"_blank\">".$sysnum."</a>";
  }else{
    // we have no good data so return an empty string
    $url = "";
  }
  
  return $url;

  }