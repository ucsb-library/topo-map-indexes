<?php
include("config/config.php");
include("lib/functions.php");

class BroadDiv extends PHPUnit_Framework_TestCase {
  public function testGetDivision() {

    // provides $correct array
    include("broad_divisions_data2.php");
    $broad_divisions = get_broad_divisions("topo-index-scans/broad_divisions.csv");

    $topos_datafile = "topo-index-scans/foreign_topo_index_scanning.csv";
    $handle = fopen($topos_datafile, "r");

    while ($data = fgetcsv($handle, 0, ",")) {
      // if ( $data[3] === "Scale" ||
      //      // skip header type spreadsheet rows which are distinguised by having no entry in the Scale column
      //      $data[3] === "" ) {
      //   continue;
      // }
      $calculatedDivs[] = get_division($data, $broad_divisions);
    }

    fclose($handle);
    $this->assertEquals($correct, $calculatedDivs);
  }
}
