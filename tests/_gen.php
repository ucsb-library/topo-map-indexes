<?php
function get_broad_divisions($broad_divisions_datafile){
  if (($handle = fopen($broad_divisions_datafile, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 9000, ",")) !== FALSE) {
      foreach ($data as $key => $value) {
        if(!is_numeric($data[0])){continue;}
        $bdsn = $data[0];
        $broad_divisions[$bdsn] = $data[1];
      }
    }
    fclose($handle);
  }else{
    die ("No data file found.");
  }
  return $broad_divisions;
}

function get_division($row_data, $broad_divisions){
  $division = '';
  $this_div = intval(substr($row_data[14], 0, 4));

  foreach($broad_divisions as $key => $value){
    $ranges[] = array('min' => $key,  'loc' => $value);
  }

  foreach($ranges as $key => $value){
    $nextkey = $key + 1;
    if(($key + 2) < count($ranges)){
      $max = $ranges[$nextkey]['min'] - 1;
    }else{
      $max = 9999;
    }
    $range2[$key]['max'] = $max;
  }
  foreach($ranges as $key => $value){
    $range3[$key] = $value;
    $range3[$key]['max'] = $range2[$key]['max'];
  }

  foreach ($range3 as $key => $value) {
    if(($this_div >= $value['min'])AND($this_div <= $value['max'])){
      $division = $value['min'];
    }
  }

  return array($row_data, $division);

}

$broad_divisions = get_broad_divisions("../topo-index-scans/broad_divisions.csv");
$topos_datafile = "../topo-index-scans/foreign_topo_index_scanning.csv";

if (!($handle = fopen($topos_datafile, "r"))) {
  echo '<p>Unable to read data.</p>';
}
else {
  echo '<?php $correct = array(';
  while ($data = fgetcsv($handle, 0, ",")) {
    // // skip CSV headers
    // if ( $data[3] === "Scale" ||
    //      // skip header type spreadsheet rows which are distinguised by having no entry in the Scale column
    //      $data[3] === "" ) {
    //   continue;
    // }
    $honk = get_division($data, $broad_divisions);

    echo '  "' . $honk[1] . "\",\n";

    // $strarr = 'array(';
    // foreach ($honk[0] as $elem) {
    //   $strarr .= '"' . $elem . '",';
    // }
    // $strarr .= ')';

    // echo '"' . $honk[1] . '" => ' . $strarr . ",\n";
  }
  echo ");";
}
